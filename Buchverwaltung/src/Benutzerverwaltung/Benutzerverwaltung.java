package Benutzerverwaltung;

import java.lang.reflect.Array;
import java.util.*;

public class Benutzerverwaltung {

	static void menue() {
		System.out.println("\n ***** Mitarbeiterverwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) Die h�chste ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//ArrayList<Buch> buchliste = new ArrayList<Buch>();
		
		LinkedList<Buch> buchliste = new LinkedList<Buch>();
		
		Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;

		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':

				System.out.println("Title: ");
				String titel = myScanner.next();
				System.out.println("ISBN: ");
				String isbn = myScanner.next();
				Buch b = new Buch(titel, isbn);

				buchliste.add(b);

				break;
			case '2':
				System.out.println("gesuchte ISBN: ");
				String isb = myScanner.next();
				System.out.println(findeBuch(buchliste, isb));

				break;
			case '3':
				System.out.println("ISBN des zu l�schenden Buches:");
				String isbnd = myScanner.next();
				loescheBuch(buchliste, isbnd);
				break;

			case '4':
				System.out.println(ermitteleGroessteISBN(buchliste));
				
				break;
			case '5':
				System.out.println(buchliste);
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			} // switch

		} while (wahl != 9);
	}// main

	
	public static Buch findeBuch(List<Buch> buchliste, String isb) {
	    
		for (Buch x : buchliste) {
			if (x.getIsbn().equals(isb))
			return x;
		}
	    return null;
	  }
	
	public static boolean loescheBuch(List<Buch> buchliste, String isbn) {
		for (Buch b1 : buchliste) {
			if (b1.getIsbn().equals(isbn)) {
				buchliste.remove(b1);
				System.out.println(b1.getIsbn()+" Erfolgreich gel�scht");
				return true;	
			}
			System.err.println("Buch nicht vorhanden");
		}return false;
	 }
	
	public static String ermitteleGroessteISBN(List<Buch> buchliste) {
	  Collections.sort(buchliste);
		return buchliste.get(buchliste.size()-1).getIsbn();
	 }

}

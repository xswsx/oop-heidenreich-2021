package Benutzerverwaltung;

public class Buch implements Comparable<Buch> {

	private String autor;
	private String title;
	private String isbn;

	public Buch(String title, String isbn) {
		super();
		this.title = title;
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	
	public boolean equals(Buch b){
		if(this.isbn.equals(b.getIsbn())==true)
		return true;
		return false;
		}
	@Override
	public int compareTo(Buch b) {
		return this.isbn.compareTo(b.getIsbn());
	}
	
	@Override 
	public String toString()
	{
		return title+isbn;
	}

}



import java.util.Scanner;

public class LS_Fahrkartenautomat_03_07 {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		while (true) {
			double sum = fahrkartenbestellungErfassen();
			// Geldeinwurf

			double r�ckgabebetrag = fahrkartenBezahlen(sum);
			// Fahrscheinausgabe
			// -----------------
			fahrkartenAusgeben();

			// R�ckgeldberechnung und -Ausgabe
			// -------------------------------
			r�ckgeldausgabe(r�ckgabebetrag, sum);
			System.out.println();
			System.out.println("N�chste Eingabe m�glich in 5 sec");
			for (int i = 0; i < 5; i++) {
				System.out.print("=");
				warte(1000);
			}
			System.out.println();

		}
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);

		int auswahl;
		int Anzahl = 0;
		int gesamt = 0;
		int auswahl2 = 0;

		do {
			Ticket[] ticketarten = { new Ticket("Einzelfahrschein Berlin AB", 2.9, 0),
					new Ticket("Einzelfahrschein Berlin BC", 3.3, 0), new Ticket("Einzelfahrschein Berlin ABC", 3.6, 0),
					new Ticket("Kurzstrecke", 1.9, 0), new Ticket("Tageskarte Berlin AB", 8.6, 0),
					new Ticket("Tageskarte Berlin BC", 9.0, 0), new Ticket("Tageskarte Berlin ABC", 9.6, 0),
					new Ticket("Kleingruppen-Tageskarte Berlin AB", 23.5, 0),
					new Ticket("Kleingruppen-Tageskarte Berlin BC", 24.3, 0),
					new Ticket("Kleingruppen-Tageskarte Berlin ABC", 24.9, 0),
					new Ticket("Kleingruppen-Tageskarte Berlin AB", 23.5, 0), new Ticket("neuesTicket", 2, 0) };

			// System.out.print(ticketarten[0].getBezeichner());
			int max = 0;
			for (int i = 0; i < ticketarten.length; i++) {

				if (max < ticketarten[i].getBezeichner().length()) {
					// System.out.println(max);
					max = ticketarten[i].getBezeichner().length();
				}
			}
			System.out.println("Fahrkarten:");
			for (int i = 0; i < ticketarten.length; i++) {
				System.out.printf("%12d   ", i + 1);
				System.out.printf("%s", ticketarten[i].getBezeichner());
				for (int j = ticketarten[i].getBezeichner().length(); j < max + 3; j++) {
					System.out.print(" ");
				}
				System.out.printf("%5.2f�", ticketarten[i].getPreis());
				System.out.println();
			}

			do {
				System.out.print("Ihre Wahl:");
				// ticket.setBezeichner(tastatur.next());
				auswahl = tastatur.nextInt();
				// ticket.setBezeichner(bez[auswahl]);
				if (auswahl < 1 || auswahl > ticketarten.length) {
					System.out.println(" >>falsche Eingabe<<");
				}

			} while (auswahl < 1 || auswahl > ticketarten.length);

			System.out.println("Anzahl der Tickets");
			Anzahl = tastatur.nextInt();
			System.out.println("Weiter(0)");
			System.out.println("Weitere Bestellungen(1)");

			auswahl2 = tastatur.nextInt();
			ticketarten[auswahl - 1].setAnzahl(Anzahl);
			// System.out.println(ticketarten[auswahl-1].getAnzahl());
			gesamt += ticketarten[auswahl - 1].getPreis() * Anzahl * 100;
		} while (auswahl2 != 0);
		return gesamt;

	}

	public static double fahrkartenBezahlen(double zuZahlen) {

		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze;
		eingezahlterGesamtbetrag = 0.0;

		System.out.printf("Noch zu zahlen: %.2f� ", (zuZahlen) / 100);
		System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");

		while (eingezahlterGesamtbetrag < zuZahlen) {
			eingeworfeneM�nze = tastatur.nextDouble() * 100;// jhgkjhgbjhfvfbjbzu
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
			if ((zuZahlen) > eingezahlterGesamtbetrag) {
				System.out.printf("Noch zu zahlen: %.2f� ", (zuZahlen - eingezahlterGesamtbetrag) / 100);
				System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			}

		}
		return eingezahlterGesamtbetrag;

	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(25);
		}
		System.out.println("\n\n");

	}

	public static void r�ckgeldausgabe(double eingezahlterGesamtbetrag, double sum) {

		double r�ckgabebetrag = (eingezahlterGesamtbetrag - sum);
		if (r�ckgabebetrag > 0) {

			System.out.printf("Der R�ckgabebetrag in H�he von: %.2f� ", r�ckgabebetrag / 100);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			muenzenAusgeben(r�ckgabebetrag, "�");

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.");
		}
		if (r�ckgabebetrag == 0) {
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.");
		}
	}

	public static void warte(int millisekunden) {
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void muenzenAusgeben(double betrag, String einheit) {
		int i = 0;
		int kasse[][] = { { 200, 100, 50, 20, 10, 5 }, { 10, 10, 10, 10, 10, 10 } };

		if (betrag >= 200) {
			i = 0;
			while (betrag >= 200) // 2 EURO-M�nzen
			{
				betrag -= 200;
				i++;
			}
			/*kasse[1][0] -= i;
			System.out.println(kasse[1][0]);
			if (i <= kasse[1][0])*/
				System.out.printf("%dx 2,00%s %n", i, einheit);

		}
		if (betrag >= 100) {
			i = 0;
			while (betrag >= 100) // 1 EURO-M�nzen
			{

				betrag -= 100;
				i++;
			}
			System.out.printf("%dx 1,00%s%n", i, einheit);
		}
		if (betrag >= 50) {
			i = 0;
			while (betrag >= 50) // 50 CENT-M�nzen
			{

				betrag -= 50;

				i++;
			}
			System.out.printf("%dx 0,50%s%n", i, einheit);
		}
		if (betrag >= 20) {
			i = 0;
			while (betrag >= 20) // 20 CENT-M�nzen
			{

				betrag -= 20;

				i++;
				;
			}
			System.out.printf("%dx 0,20%s%n", i, einheit);
		}
		if (betrag >= 10) {
			i = 0;
			while (betrag >= 10) // 10 CENT-M�nzen
			{

				betrag -= 10;

				i++;
			}
			System.out.printf("%dx 0,10%s%n", i, einheit);
		}
		if (betrag >= 5) {
			i = 0;
			while (betrag >= 5)// 5 CENT-M�nzen
			{
				betrag -= 5;
				i++;
			}
			System.out.printf("%dx 0,05%s%n", i, einheit);
		}

	}

	public boolean Kasse(int sum) {

		return true;
	}
}


public class AB_Array_04a {
	public static void main(String[] args) {
		
			System.out.println("Die Lottozahlen sind:");

			int zahlen[] = { 3, 7, 12, 18, 37, 42 };

			System.out.print("[");
			for (int i = 0; i < zahlen.length; i++) {
				System.out.print(zahlen[i] + " ");
			}
			System.out.print("]");

			System.out.println();
			System.out.println();	
	}
}

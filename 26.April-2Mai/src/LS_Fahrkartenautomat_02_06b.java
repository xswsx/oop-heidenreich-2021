import java.util.Scanner;

public class LS_Fahrkartenautomat_02_06b {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		while (true) {
			double sum = fahrkartenbestellungErfassen();
			// Geldeinwurf

			double r�ckgabebetrag = fahrkartenBezahlen(sum);
			// Fahrscheinausgabe
			// -----------------
			fahrkartenAusgeben();

			// R�ckgeldberechnung und -Ausgabe
			// -------------------------------
			r�ckgeldausgabe(r�ckgabebetrag, sum);
			for (int i = 0; i <= 9; i++) {
				System.out.println();
			}

		}
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);

		int auswahl;
		double anzahl;
		double zuZahlen = 0;

		System.out.println("W�hlen Sie Ihre Wunschfahrkarte f�r Berlin AB aus:");
		System.out.println("  Einzelfahrschein Regeltarif AB [2,90] (1)");
		System.out.println("  Tageskarte Rewgeltarif AB [8,60] (2)");
		System.out.println("  Kleingruppen-Tageskarte Regeltarif AB [23,50] (3)");
		System.out.println();

		do {
			System.out.print("Ihre Wahl:");
			auswahl = tastatur.nextInt();
			if (auswahl < 1 || auswahl > 3) {
				System.out.println(" >>falsche Eingabe<<");
			}

		} while (auswahl < 1 || auswahl > 3);

		System.out.println("Anzahl der Tickets");
		anzahl = tastatur.nextDouble();

		switch (auswahl) {
		case 1:
			zuZahlen = 290;
			break;
		case 2:
			zuZahlen = 860;
			break;
		case 3:
			zuZahlen = 2350;
			break;
		}

		return zuZahlen * anzahl;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {

		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze;
		eingezahlterGesamtbetrag = 0.0;

		System.out.printf("Noch zu zahlen: %.2f� ", (zuZahlen / 100));
		System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");

		while (eingezahlterGesamtbetrag < zuZahlen) {
			eingeworfeneM�nze = tastatur.nextDouble() * 100;// jhgkjhgbjhfvfbjbzu
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
			if ((zuZahlen) > eingezahlterGesamtbetrag) {
				System.out.printf("Noch zu zahlen: %.2f� ", (zuZahlen - eingezahlterGesamtbetrag) / 100);
				System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			}

		}
		return eingezahlterGesamtbetrag;

	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(25);
		}
		System.out.println("\n\n");

	}

	public static void r�ckgeldausgabe(double eingezahlterGesamtbetrag, double sum) {

		double r�ckgabebetrag = (eingezahlterGesamtbetrag - sum);
		if (r�ckgabebetrag > 0) {

			System.out.printf("Der R�ckgabebetrag in H�he von: %.2f� ", r�ckgabebetrag / 100);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			muenzenAusgeben(r�ckgabebetrag, "�");

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.");
		}
		if (r�ckgabebetrag == 0) {
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.");
		}
	}

	public static void warte(int millisekunden) {
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void muenzenAusgeben(double betrag, String einheit) {
		int i = 0;

		if (betrag >= 200) {
			i = 0;
			while (betrag >= 200) // 2 EURO-M�nzen
			{
				betrag -= 200;
				i++;
			}
			System.out.printf("%dx 2,00%s %n", i, einheit);

		}
		if (betrag >= 100) {
			i = 0;
			while (betrag >= 100) // 1 EURO-M�nzen
			{

				betrag -= 100;
				i++;
			}
			System.out.printf("%dx 1,00%s%n", i, einheit);
		}
		if (betrag >= 50) {
			i = 0;
			while (betrag >= 50) // 50 CENT-M�nzen
			{

				betrag -= 50;

				i++;
			}
			System.out.printf("%dx 0,50%s%n", i, einheit);
		}
		if (betrag >= 20) {
			i = 0;
			while (betrag >= 20) // 20 CENT-M�nzen
			{

				betrag -= 20;

				i++;
				;
			}
			System.out.printf("%dx 0,20%s%n", i, einheit);
		}
		if (betrag >= 10) {
			i = 0;
			while (betrag >= 10) // 10 CENT-M�nzen
			{

				betrag -= 10;

				i++;
			}
			System.out.printf("%dx 0,10%s%n", i, einheit);
		}
		if (betrag >= 5) {
			i = 0;
			while (betrag >= 5)// 5 CENT-M�nzen
			{
				betrag -= 5;
				i++;
			}
			System.out.printf("%dx 0,05%s%n", i, einheit);
		}

	}
}

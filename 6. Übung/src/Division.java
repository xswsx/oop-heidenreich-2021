
public class Division {

	public static void main(String[] args) {
		
		try {
			System.out.print(div(8,1));
		}
		catch (ArithmeticException ex)
		{
			System.out.println(ex.getMessage());
		}
	}

	public static double div(int z, int n) throws  ArithmeticException {
		if (n == 0)
			throw new  ArithmeticException("Division mit 0 nicht m�glich!");
		return z / n;
		
	}
}

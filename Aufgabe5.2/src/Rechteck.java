
public class Rechteck extends GeoObject {
	
	private int h;
	private int b;
	
	public Rechteck(double x, double y, int h, int b) {
		super(x, y);
		this.h = h;
		this.b = b;
	}
	
	public double determineArea() {

		return  this.h* this.b;
	}

	@Override
	public String toString() {
		return "Rechteck: h=" + h + ", b="+b + super.toString()+", Fl�che: "+ determineArea();
	}
	
	

}


public class Kreis extends GeoObject {

	private int r;
	private static final double pi = 3.1415;

	public Kreis(double x, double y, int r) {
		super(x, y);
		this.r = r;
	}

	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = r;
	}

	public static double getPi() {
		return pi;
	}

	public double determineArea() {

		return  this.r* this.r*pi;
	}

	@Override
	public String toString() {
		return "Kreisradius: " + r  + super.toString()+", Fl�che: "+ determineArea();
	}
	
	

}


public class Date {

	private int day;
	private int month;
	private int year;
	private static int anzahl = 0;

	public Date(int day, int month, int year) throws wrongday, wrongmonth,wrongyear {
		setday(day);
		setmonth(month);
		setyear( year);
		anzahl++;

	}

	public Date() {
		this.day = 24;
		this.month = 12;
		this.year = 2014;
		anzahl++;
	}

	public int getday() {
		return day;
	}

	public void setday(int day) throws wrongday {

		if (day < 1 || day > 31)
			throw new wrongday("falscher Tag");
		this.day = day;
	}

	public int getmonth() {
		return month;
	}

	public void setmonth(int month) throws wrongmonth {
		if (month < 1 || month > 12)
			throw new wrongmonth("falscher Monat");
		this.month = month;
	}

	public int getyear() {
		return year;
	}

	public void setyear(int year) throws wrongyear {
		
		if (year < 1900 || year > 2100)
			throw new wrongyear("falsches Jahr");
		this.year = year;
	}

	public String toString() {
		return day + "." + month + "." + year + " "; // !!!!!!!!!!
	}

	public boolean equals(Object obj) {
		Date date = (Date) obj;
		if (this.day == date.getday() && this.month == date.getmonth() && this.year == date.getyear())
			return true;
		else
			return false;

	}

	public static int calcQuartal(Date date) {
		int q = 0;
		if (date.getmonth() >= 1 && date.getmonth() <= 3) {
			q = 1;
		}

		if (date.getmonth() > 3 && date.getmonth() <= 6) {
			q = 2;
		}

		if (date.getmonth() > 6 && date.getmonth() <= 9) {
			q = 3;
		}

		if (date.getmonth() > 9 && date.getmonth() <= 12) {
			q = 4;
		}

		return q;
	}
}


public class Feiertag extends Date {
	
	private String Feiertag;

	public String getFeiertag() {
		return Feiertag;
	}

	public void setFeiertag(String feiertag) {
		Feiertag = feiertag;
	}

	public Feiertag(int day, int month, int year, String feiertag) {
		super(day, month, year);
		this.Feiertag = feiertag;
	}

	@Override
	public String toString() {
		return super.toString() + "ist "+  this.Feiertag;
	}
	
	

}

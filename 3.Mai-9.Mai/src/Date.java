
public class Date {

	private int day;
	private int month;
	private int year;
	private static int anzahl = 0;

	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
		anzahl++;

	}

	public Date() {
		this.day = 1;
		this.month = 1;
		this.year = 1970;
		anzahl++;
	}

	public int getday() {
		return day;
	}

	public void setday(int day) {
		this.day = day;
	}

	public int getmonth() {
		return month;
	}

	public void setmonth(int month) {
		this.month = month;
	}

	public int getyear() {
		return year;
	}

	public void setyear(int year) {
		this.year = year;
	}

	public String toString() {
		return  day + "." + month + "." + year; // !!!!!!!!!!
	}

	public boolean equals(Object obj) {
		Date date = (Date) obj;
		if (this.day == date.getday() && this.month == date.getmonth() && this.year == date.getyear())
			return true;
		else
			return false;

	}
	public static int calcQuartal(Date date)
	{
		int q=0;
		if (date.getmonth()>=1 && date.getmonth()<=3)
		{
			q=1;
		}
		
		if (date.getmonth()>3 && date.getmonth()<=6)
		{
			q=2;
		}
		
		if (date.getmonth()>6 && date.getmonth()<=9)
		{
			q=3;
		}
		
		if (date.getmonth()>9 && date.getmonth()<=12)
		{
			q=4;
		}
		
		return q;
	}
}

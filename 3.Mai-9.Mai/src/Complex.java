
public class Complex {
	private double Re;
	private double Im;

	public Complex(double Re, double Im) {
		this.Re = Re;
		this.Im = Im;
	}

	public Complex() {
		this.Re = 0.0;
		this.Im = 0.0;
	}

	public double getRe() {
		return Re;
	}

	public void setRe(double Re) {
		this.Re = Re;
	}

	public double getIm() {
		return Im;
	}

	public void setIm(double Im) {
		this.Im = Im;
	}

	public static Complex complexcalc(Complex a, Complex b) {

		double re = a.getRe() * b.getRe() - a.getIm() * b.getIm();
		double im = a.getRe() * (b.getIm() + b.getRe() * a.getIm());

		return new Complex(re, im);
	}

	public Complex complexcalc2(Object obj) {
		Complex comp = (Complex) obj;
		return new Complex(comp.getRe() * this.Re - comp.getIm() * this.Im,
				comp.getRe() * this.Im + this.Re * comp.getIm());
	}
}

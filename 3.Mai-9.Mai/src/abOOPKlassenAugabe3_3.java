
public class abOOPKlassenAugabe3_3 {

	public static void main(String[] args) {

		Complex zahl1 = new Complex(-1, -2);
		Complex zahl2 = new Complex(1, 2);
		Complex zahl3 = new Complex();

		// System.out.println(zahl1.getRe());
		// System.out.println(zahl1.getIm());

		// System.out.printf("%f",complexcalc(zahl1,zahl2));

		zahl3 = Complex.complexcalc(zahl1, zahl2); // Klassenmethode

		zahl3 = zahl1.complexcalc2(zahl2); // Objectmethode

		System.out.print(zahl3.getRe());
		System.out.print(" + ");
		System.out.print(zahl3.getIm());
		System.out.print("j");
	}

}

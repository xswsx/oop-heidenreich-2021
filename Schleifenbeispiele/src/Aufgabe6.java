import java.util.Scanner;

public class Aufgabe6 {
	public static void main(String[] args) {
		Scanner keyin = new Scanner(System.in);
		System.out.println("wieviel Stufen?");
		int stufen = keyin.nextInt();

		for (int i = 1; i <= stufen; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}

}

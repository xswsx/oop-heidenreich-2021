﻿﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double sum= fahrkartenbestellungErfassen();
       // Geldeinwurf
       // -----------
       double rückgabebetrag = fahrkartenBezahlen(fahrkartenbestellungErfassen());
       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgeldausgabe(rückgabebetrag,sum);
    }
    public static double fahrkartenbestellungErfassen()
	{
    	Scanner tastatur = new Scanner(System.in);
        
        double zuZahlenderBetrag; 
        double anzahl;
        
        System.out.print("Ticketpreis (EURO-Cent): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        System.out.print("Anzahl der Tickets: ");
        anzahl = tastatur.nextDouble();
         
        return zuZahlenderBetrag*anzahl;
	}
    public static double fahrkartenBezahlen(double zuZahlen)
    {
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMünze;
    	eingezahlterGesamtbetrag = 0.0;
        
 	   System.out.printf("Noch zu zahlen: %.2f€ " ,zuZahlen);
 	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    
 	   while(eingezahlterGesamtbetrag < zuZahlen)
    { 
 	   eingeworfeneMünze = tastatur.nextDouble();
 	   eingezahlterGesamtbetrag += eingeworfeneMünze;
 	   if ((zuZahlen)>eingezahlterGesamtbetrag)
 	   {
 	   System.out.printf("Noch zu zahlen: %.2f€ " ,(zuZahlen-eingezahlterGesamtbetrag));
 	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
 	   }
             
    }
 	   return eingezahlterGesamtbetrag;
    }
    public static void fahrkartenAusgeben()
    {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    	
    }
    public static void rückgeldausgabe (double eingezahlterGesamtbetrag, double sum)
    {
    	double rückgabebetrag=0.0;
    	rückgabebetrag = eingezahlterGesamtbetrag - sum;
        if(rückgabebetrag > 0.0)
        {
     	   float i=0;
     	   int rück = (int)(rückgabebetrag*100) ;
     	   
     	   System.out.printf("Der Rückgabebetrag in Höhe von: %.2f€ ", rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

     	  muenzenAusgeben(rück, "€");
     	   /*if(rück >= 200)
     	   {i=0;
     		   while(rück >= 200) // 2 EURO-Münzen
                {  
             	  
     	          rück -= 200;	
     	          i++;
                }         
                System.out.printf("%.0fx2 EURO%n",i); 
     	   }
            */
     	   

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
     }
    }
    public static void warte (int millisekunden)
    {
    try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public static void muenzenAusgeben (int betrag, String einheit)
    { int i=0;
    if(betrag >= 200)
    {
    	i=0;
		   while(betrag >= 200) // 2 EURO-Münzen
         {  
	          betrag -= 200;	
	          i++;
         }         
         System.out.printf("%dx 2,00%s %n",i,einheit); 
    	
	   }
    	if(betrag >= 100)
  	   {i=0;
  		   while(betrag >= 100) // 1 EURO-Münzen
             {  
          	  
  			 betrag -= 100;
  	          i++;
             }
             System.out.printf("%dx 1,00%s%n", i,einheit);
  	   }
  	   if(betrag >= 50)
  	   {i=0;
  		   while(betrag >= 50) // 50 CENT-Münzen
             {  
          	  
  			 betrag -= 50;
  	          i++;
             }
             System.out.printf("%dx 0,50%s%n", i,einheit);
  	   }
  	   if(betrag >= 20)
  	   {   i=0;
  		   while(betrag >= 20) // 20 CENT-Münzen
             {   
          	  
  			 betrag -= 20;
   	          i++;;
             }
             System.out.printf("%dx 0,20%s%n", i,einheit);
  	   }
  	   if(betrag >= 10)
  	   {i=0;
  		   while(betrag >= 10) // 10 CENT-Münzen
             {  
          	  
  			 betrag -= 10;
  	          i++;
             }
             System.out.printf("%dx 0,10%s%n", i,einheit);
  	   }
  	   if(betrag >= 5)
  	   {i=0;
  		   while(betrag >= 5)// 5 CENT-Münzen
             {  
  			 betrag -= 5;
   	          i++;
             }
             System.out.printf("%dx 0,05%s%n", i,einheit);
  	   }
         
     }
    }


import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {

		// Benutzereingaben lesen
		String was = liesString("Was m�chten Sie bestellen:");
		int meng = liesInt("geben Sie die Menge an:");
		double preis = liesDouble("Prei in �:");
		double steuer = liesDouble("Mehrwertsteuer in %:");
		// Verarbeiten
		double nettopreis = berechneGesamtnettopreis(meng, preis);
		double brutto = berechneGesamtbruttopreis(nettopreis, steuer);
		// Ausgeben
		rechungausgeben(was,meng,nettopreis,brutto,steuer);

	}
	public static String liesString(String text)
	{
    
	System.out.println(text);
	Scanner myScanner = new Scanner(System.in);
	String artikel = myScanner.next();
	return artikel;
	}
	
	public static int liesInt(String text)
	{
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}
	
	public static double liesDouble(String text)
	{
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double preis = myScanner.nextDouble();
		return preis;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis)
	{
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst)
	{
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,double mwst)
	{
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}
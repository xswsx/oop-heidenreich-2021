import java.util.Scanner;

public class Aufgabe4 {
	public static void main(String[] args) {
		while (true) {
			System.out.print("Gesuchte Zahl:");
			Scanner keyin = new Scanner(System.in);
			int searchfor = keyin.nextInt();
			boolean x = false;

			int zahlen[] = { 3, 7, 12, 18, 37, 42 };

			/*for (int i = 0; i < zahlen.length; i++) {
				System.out.print(zahlen[i] + " ");
			}*/
			System.out.println();
			for (int i = 0; i < zahlen.length; i++) {
				if ((zahlen[i]) == searchfor) {
					x = true;
				}
			}

			if (x == false) {
				System.out.printf("Die Zahl %d ist in der Ziehung enthalten ", searchfor);
			}

			if (x) {
				System.out.printf("Die Zahl %d ist nicht in der Ziehung enthalten ", searchfor);
			}
			System.out.println();
			System.out.println();
		}
	}
}

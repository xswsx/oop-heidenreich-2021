package myUtil;

import java.util.Arrays;

public class MyIntArray implements IIntArray {

	private int[] array;

	public MyIntArray(int l�nge) {

		this.array = new int[l�nge];
	}
	
	

	public int get(int index) throws MyIndexException {

		if (this.array.length <= index && this.array.length >= 0)
			return this.array[index];
		else
			throw new MyIndexException(index);
	}

	public void set(int index, int value) throws MyIndexException

	{
		if (index >= 0 && index < this.array.length)
			this.array[index] = value;
		else
			throw new MyIndexException(index);
	}

	public void increase(int n) {
		int[] newarray = new int[this.array.length + n];
		for (int i = 0; i < this.array.length; i++)
		newarray[i] = this.array[i];
		this.array = newarray;
		}
	
	

	@Override
	public String toString() {
		return "MyIntArray = " + Arrays.toString(array);
	}

}

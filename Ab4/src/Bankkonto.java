import java.util.Scanner;

public class Bankkonto {
	Scanner keyin = new Scanner(System.in);
	private String Inhaber;
	private String KtoNr;
	private double Kontostand;

	public String getInhaber() {
		return Inhaber;
	}

	public Bankkonto(String inhaber, String ktoNr, double kontostand) {
		Inhaber = inhaber;
		KtoNr = ktoNr;
		Kontostand = kontostand;
	}

	public void setInhaber(String inhaber) {
		Inhaber = inhaber;
	}

	public String getKtoNr() {
		return KtoNr;
	}

	public void setKtoNr(String ktoNr) {
		KtoNr = ktoNr;
	}

	public double getKontostand() {
		return Kontostand;
	}

	public void setKontostand(double kontostand) {
		Kontostand = kontostand;
	}

	public void einzahlen() {
		System.out.println("Bitte legen Sie das Geld ein");
		double money = keyin.nextInt();
		this.Kontostand += money;

	}
	
	public double auszahlen() {
		
		System.out.println("Welchen Betrag wollen Sie erhalten?");
		 double money = keyin.nextInt();

		this.Kontostand -= money;
		
		return money;
	}

	public String toString() {
		return String.format ("\nInhaber:     %s \nKtoNr:       %s \nKontostand:  %.2f� ", this.Inhaber,this.KtoNr, this.Kontostand);
	}

}


public class Dispokonto extends Bankkonto {

	private double Dispokredit;

	public Dispokonto(String inhaber, String ktoNr, double kontostand, double dispokredit) {
		super(inhaber, ktoNr, kontostand);
		Dispokredit = dispokredit;
	}

	public double getDispokredit() {
		return Dispokredit;
	}

	public void setDispokredit(double dispokredit) {
		Dispokredit = dispokredit;
	}

	@Override
	public double auszahlen() {
		System.out.println("Welchen Betrag wollen Sie erhalten?");
		double money = keyin.nextInt();

		if (this.getKontostand() + Dispokredit >= money) {
			this.setKontostand(this.getKontostand() - money);
		} else if (this.getKontostand() + Dispokredit < money) {
			System.out.println("nicht genug Guthaben verf�gbar");
			System.out.printf("es k�nnen max. %.2f� ausgezahlt werden", (this.getKontostand() + Dispokredit));
			money = this.getKontostand() + Dispokredit;
			this.setKontostand(this.getKontostand() - money);
		}

		return money;
	}

	@Override
	public String toString() {
		return String.format ("%s\nDispokredit: %.2f�",super.toString(),this.Dispokredit );
	}

}

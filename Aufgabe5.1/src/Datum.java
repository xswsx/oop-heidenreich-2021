
public class Datum implements Comparable<Datum> {
	private int tag;
	private int monat;
	private int jahr;

	public Datum() {
		this.tag = 1;
		this.monat = 1;
		this.jahr = 1970;
	}

	public Datum(int tag, int monat, int jahr) throws wrongday, wrongmonth, wrongyear {

		if (tag < 1 || tag > 31)
			throw new wrongday("falscher Tag", tag);
		if (monat < 1 || monat > 12)
			throw new wrongmonth("falscher Monat", monat);
		if (jahr < 1900 || jahr > 2100)
			throw new wrongyear("falsches Jahr", jahr);

		this.tag = tag;
		this.monat = monat;
		this.jahr = jahr;

	}

	public int getTag() {
		return tag;
	}

	public void setTag(int tag) throws wrongday {
		this.tag = tag;

		if (tag < 1 || tag > 31)
			throw new wrongday("falscher Tag", tag);
	}

	public int getMonat() {
		return monat;
	}

	public void setMonat(int monat) throws wrongmonth {
		this.monat = monat;
		if (monat < 1 || monat > 12)
			throw new wrongmonth("falscher Monat", monat);
	}

	public int getJahr() {
		return jahr;
	}

	public void setJahr(int jahr) throws wrongyear {

		if (jahr < 1900 || jahr > 2100)
			throw new wrongyear("falsches Jahr", jahr);
		this.jahr = jahr;
	}

	public static int berechneQuartal(Datum d) {
		return d.monat / 3 + 1;
	}

	@Override
	public boolean equals(Object obj) {
		Datum d = (Datum) obj;
		if (this.tag == d.tag && this.monat == d.monat && this.jahr == d.jahr)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return this.tag + "." + this.monat + "." + this.jahr;
	}

	public int compareTo(Datum d) {

		if (this.monat < d.getMonat()) {
			return ((this.jahr - d.getJahr())) * 365 + (this.monat * 30 - d.getMonat() * 30) + (this.tag - d.getTag())
					- 1;
		}

		if (this.monat > d.getMonat()) {
			return ((this.jahr - d.getJahr())) * 365 + (this.monat * 30 - d.getMonat() * 30) + (this.tag - d.getTag())
					+ 1;
		}

		return ((this.jahr - d.getJahr())) * 365 + (this.monat * 30 - d.getMonat() * 30) + (this.tag - d.getTag());

	}
}

public class DatumMain {

	public static void main(String[] args) {

		try {
			Datum d1 = new Datum(1, 33, 2000);
			Datum d2 = new Datum(1, 2, 2020);
			
			System.out.println(d1);
			
		}

		catch (wrongday ex) {
			System.out.printf("%s!\nEs gibt keinen %d. Tag ", ex.getMessage(), ex.getX());
		} catch (wrongmonth ex) {
			System.out.printf("%s!\nEs gibt keinen %d. Monat", ex.getMessage(), ex.getX());
		} catch (wrongyear ex) {
			System.out.printf("%s: %d", ex.getMessage(), ex.getX());
		}

	}

}

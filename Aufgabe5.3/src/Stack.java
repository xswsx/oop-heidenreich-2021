
public class Stack implements IntStack {

	private static int index = 0;

	private int[] speicher;

	public Stack(int l�nge) {

		this.speicher = new int[l�nge];

	}

	@Override
	public int pop() {

		if (index >= 0)
			index--;
		return speicher[index];
	}

	@Override
	public void push(int x) {

		speicher[index] = x;
		if (index < speicher.length)
			index++;

	}

	public static int getIndex() {
		return index;
	}

	public static void setIndex(int index) {
		Stack.index = index;
	}

	public int[] getSpeicher() {
		return speicher;
	}

	public void setSpeicher(int[] speicher) {
		this.speicher = speicher;
	}

	public void showmemory() {
		for (int i = 0; i < index; i++)
			System.out.print(speicher[i]);

		System.out.println();

	}

}

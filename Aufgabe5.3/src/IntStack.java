
public interface IntStack {
	
	int pop();
	void push(int x);

}

import java.util.Arrays;

public class Ticket {
private String[] bezeichner = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
		"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC",
		"Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
		"Kleingruppen-Tageskarte Berlin ABC", "BerlinForEver", "neue Fahrkarte" };

private double [] preis={ 290, 330, 360, 190, 860, 900, 960, 2350, 2430, 2490, 100, 840 };
private int anzahl;

public Ticket(String[] bezeichner, double[] preis, int anzahl) {
	this.bezeichner = bezeichner;
	this.preis = preis;
	this.anzahl = anzahl;
}




public String[] getBezeichner() {
	return bezeichner;
}

public void setBezeichner(String[] bezeichner) {
	this.bezeichner = bezeichner;
}

public double[] getPreis() {
	return preis;
}

public void setPreis(double[] preis) {
	this.preis = preis;
}

public int getAnzahl() {
	return anzahl;
}

public void setAnzahl(int anzahl) {
	this.anzahl = anzahl;
}

@Override
public String toString() {
	return "Ticket [bezeichner=" + Arrays.toString(bezeichner) + ", preis=" + Arrays.toString(preis) + ", anzahl="
			+ anzahl + "]";
}








}
